#!/bin/bash

echo "Ensuring Mac Developer Tools Package is Installed..."
xcode-select --install &> /dev/null && read -n 1 -s -r -p "Press ENTER to continue once developer tools are installed."

echo "Pulling down docksal.cloud..."

if `git clone git@gitlab.com:christophersmith262/pega-docksal.cloud.git docksal.cloud`
then
    cd docksal.cloud && ./quickstart.sh
else
    echo ""
    echo "Could not download the quickstart script. Is your ~/.ssh/id_rsa key set up with access to the Pegasystems organization at https://gitlab.com?"
    exit 1
fi
